#!/bin/bash

echo -n "Enter the namespace to teardown: "
read namespace

kubectl delete --namespace=$namespace rc/wdts-nginx
kubectl delete --namespace=$namespace svc/wdts-nginx

echo -n Waiting for all pods to terminate
while [ -n "`kubectl get pods  --selector=app=wdts-nginx --namespace=$namespace`" ]; do
  echo -n .
  sleep 3
done
echo
echo All pods have terminated

# remove namespace
#kubectl delete namespace/$namespace
