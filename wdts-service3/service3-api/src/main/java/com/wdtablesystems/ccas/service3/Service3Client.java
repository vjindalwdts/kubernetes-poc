package com.wdtablesystems.ccas.service3;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * Created by vjindal on 10/17/16.
 */
@FeignClient("service3")
public interface Service3Client {

    @RequestMapping(method = RequestMethod.GET, value = "/api/service3/v1/greeting")
    public String greet(@RequestParam(value = "name") String name);
}
