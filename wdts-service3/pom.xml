<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>com.wdtablesystems.ccas</groupId>
    <artifactId>service3</artifactId>
    <version>5.1.0-SNAPSHOT</version>
    <name>Service3 Service</name>
    <packaging>pom</packaging>

    <parent>
        <groupId>org.springframework.boot</groupId>
        <artifactId>spring-boot-starter-parent</artifactId>
        <version>1.3.7.RELEASE</version>
        <relativePath/>
        <!-- lookup parent from repository -->
    </parent>

    <properties>
        <gmavenplus.version>1.5</gmavenplus.version>
        <groovy.version>2.3.11</groovy.version>
        <jackson.version>2.8.2</jackson.version>
        <jacoco.version>0.7.7.201606060606</jacoco.version>
        <java.source.level>1.8</java.source.level>
        <java.target.level>1.8</java.target.level>
        <java.version>1.8</java.version>
        <jettison.version>1.3.7</jettison.version>
        <maven.surefire.version>2.18</maven.surefire.version>
        <mybatis.version>3.3.0</mybatis.version>
        <mybatis-spring.version>1.2.3</mybatis-spring.version>
        <netflix.archaius.version>0.4.1</netflix.archaius.version>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
        <rxjava.version>1.0.7</rxjava.version>
        <slf4j.version>1.7.0</slf4j.version>
        <!-- Sonar, needed for combining jacoco reports between submodules -->
        <!-- This section can be omitted in simple projects with no submodules -->
        <sonar.java.coveragePlugin>jacoco</sonar.java.coveragePlugin>
        <sonar.dynamicAnalysis>reuseReports</sonar.dynamicAnalysis>
        <sonar.jacoco.reportPath>${project.basedir}/../target/jacoco.exec</sonar.jacoco.reportPath>
        <sonar.language>java</sonar.language>
        <spock.version>1.0-groovy-2.3</spock.version>
        <springframework.cloud.version>Brixton.SR5</springframework.cloud.version>
        <spring-hateoas.version>0.19.0.RELEASE</spring-hateoas.version>
        <tomcat-jdbc.version>8.0.30</tomcat-jdbc.version>
    </properties>

    <modules>
        <module>service3-api</module>
        <module>service3-ms</module>
    </modules>

    <dependencyManagement>
        <dependencies>
            <dependency>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-starter-parent</artifactId>
                <version>${springframework.cloud.version}</version>
                <type>pom</type>
                <scope>import</scope>
            </dependency>
        </dependencies>
    </dependencyManagement>

    <repositories>
        <repository>
            <id>nexus</id>
            <name>WDTS Releases Repository</name>
            <url>http://nex-01.wdtablesystems.com:8081/nexus/content/repositories/${wdts.repo.name.releases}</url>
            <layout>default</layout>
            <releases>
                <updatePolicy>always</updatePolicy>
            </releases>
        </repository>
        <repository>
            <id>nexus-thirdparty</id>
            <name>WDTS 3rd Party Repository</name>
            <url>http://nex-01.wdtablesystems.com:8081/nexus/content/repositories/thirdparty</url>
            <layout>default</layout>
            <releases>
                <updatePolicy>always</updatePolicy>
            </releases>
        </repository>
        <repository>
            <id>nexus-snapshots</id>
            <name>WDTS Snapshots Repository</name>
            <url>http://nex-01.wdtablesystems.com:8081/nexus/content/repositories/${wdts.repo.name.snapshots}</url>
            <layout>default</layout>
            <snapshots>
                <enabled>true</enabled>
                <updatePolicy>always</updatePolicy>
            </snapshots>
        </repository>
        <repository>
            <id>Central Maven</id>
            <url>http://repo1.maven.org/maven2/</url>
        </repository>
        <repository>
            <id>java.net</id>
            <url>https://maven.java.net/content/repositories/public/</url>
        </repository>
        <!-- For Spring Cloud Brixton build - remove once Brixton is released! -->
        <repository>
            <id>spring-milestones</id>
            <name>Spring Milestones</name>
            <url>http://repo.spring.io/milestone</url>
            <snapshots>
                <enabled>false</enabled>
            </snapshots>
        </repository>
    </repositories>

    <distributionManagement>
        <repository>
            <id>nexus</id>
            <name>Internal Releases</name>
            <url>http://nex-01.wdtablesystems.com:8081/nexus/content/repositories/${wdts.repo.name.releases}</url>
        </repository>
        <snapshotRepository>
            <id>nexus-snapshots</id>
            <name>Internal Snapshots</name>
            <url>http://nex-01.wdtablesystems.com:8081/nexus/content/repositories/${wdts.repo.name.snapshots}</url>
        </snapshotRepository>
    </distributionManagement>

    <build>
        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.5.1</version>
                <configuration>
                    <source>${java.source.level}</source>
                    <target>${java.target.level}</target>
                    <encoding>UTF-8</encoding>
                    <showDeprecation>true</showDeprecation>
                    <showWarnings>true</showWarnings>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-deploy-plugin</artifactId>
                <version>2.8.2</version>
                <configuration>
                    <skip>false</skip>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.jacoco</groupId>
                <artifactId>jacoco-maven-plugin</artifactId>
                <version>${jacoco.version}</version>
                <!-- Configure jacoco to append multiple reports together -->
                <!-- This section can be omitted in simple projects with no submodules -->
                <configuration>
                    <destFile>${sonar.jacoco.reportPath}</destFile>
                    <append>true</append>
                </configuration>
                <executions>
                    <!--
                    Prepares the property pointing to the JaCoCo runtime agent which
                    is passed as VM argument when Maven the Surefire plugin is executed. -->
                    <execution>
                        <id>default-prepare-agent</id>
                        <goals>
                            <goal>prepare-agent</goal>
                        </goals>
                    </execution>
                    <!--
                    Ensures that the code coverage report for unit tests is created after
                    unit tests have been run.
                    This step is only needed for local analysis, Jenkins does not rely on it -->
                    <execution>
                        <id>default-report</id>
                        <phase>prepare-package</phase>
                        <goals>
                            <goal>report</goal>
                        </goals>
                        <!-- Configure jacoco to append multiple reports together -->
                        <!-- This section can be omitted in simple projects with no submodules -->
                        <configuration>
                            <!-- Sets the path to the file which contains the execution data. -->
                            <dataFile>${sonar.jacoco.reportPath}</dataFile>
                            <!-- Sets the output directory for the code coverage report. -->
                            <outputDirectory>${project.reporting.outputDirectory}/jacoco-coverage-report</outputDirectory>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>master</id>
            <properties>
                <wdts.repo.name.releases>master-releases</wdts.repo.name.releases>
                <wdts.repo.name.snapshots>master-snapshots</wdts.repo.name.snapshots>
            </properties>
        </profile>
    </profiles>
</project>
