package com.wdtablesystems.ccas.service3;

import com.wdtablesystems.ccas.service1.Service1Client;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vjindal on 10/17/16.
 */
@Controller
@RequestMapping(value = "/api/service3/v1")
public class Service3Controller {

    @Autowired
    private Service1Client service1Client;

    @RequestMapping(method = RequestMethod.GET, value = "/greeting")
    public ResponseEntity<String> greet(@RequestParam(value = "name" ,required = false) String name) {
        String ret = "Hello " + name + " from Service3";
        ResponseEntity responseEntity = new ResponseEntity(ret, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/doublegreeting")
    public ResponseEntity<String> doubleGreet(@RequestParam(value = "name") String name) {
        String ret = "Hello " + name + " from Service3 And " + service1Client.greet(name);
        ResponseEntity responseEntity = new ResponseEntity(ret, HttpStatus.OK);
        return responseEntity;
    }
}
