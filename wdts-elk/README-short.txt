Collect, search and visualise log data with ELK (Elasticsearch 5.0.0, Logstash 5.0.0, Kibana 5.0.0).

docker run -e ES_JAVA_OPTS="-Xms1g -Xmx1g"  -p 5601:5601 -p 9200:9200 -p 5044:5044 -it --name elk 192.168.99.100:80/wdts-elk_7