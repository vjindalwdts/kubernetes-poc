#!/bin/bash
minikube ssh sudo 'mkdir -p /elk-data'
minikube ssh sudo 'chmod -R 777 /elk-data'
minikube ssh sudo 'sysctl -w vm.max_map_count=262144'
echo -n "Enter the namespace to use for deploying Service1: "
read namespace
namespaces=`kubectl get namespace -o jsonpath={..name}`
if ! [[ $namespaces =~ "$namespace" ]]; then
  echo "Setting up namespace: $namespace"
  kubectl create -f - <<EOF
apiVersion: v1
kind: Namespace
metadata:
  name: "$namespace"
EOF
else
  echo "Namespace '$namespace' already exists"
fi
echo

# Deploy zookeeper service
services=`kubectl get svc --namespace=$namespace`
if [[ $services =~ "wdts-elk" ]]; then
  echo Deleting existing services
  kubectl delete --namespace=$namespace svc/wdts-elk
fi
cat kubernetes/service.yml | sed "s/_NAMESPACE_/$namespace/" | kubectl create -f -

# Deploy zookeeper replication controllers
cat kubernetes/rc.yml | sed "s/_NAMESPACE_/$namespace/" | kubectl create -f -

# Check that zoo1, zoo2 and zoo3 are running pods
# Wait for all zookeeper pods to be ready
for i in `kubectl get pods --selector=app=wdts-elk --namespace=$namespace -o jsonpath='{.items[*].metadata.name}'`;
do
  echo -n "Waiting for pod ${i} to be ready"
  sleep 2 # Give container a chance to start
  while [ `kubectl get pod/${i} --namespace=$namespace -o jsonpath='{..status.containerStatuses[*].ready}'` != "true" ]; do
    echo -n .
    sleep 5
  done
  echo
  echo pod ${i} ready
done
