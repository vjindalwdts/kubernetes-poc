#!/bin/bash

echo -n "Enter the namespace to teardown: "
read namespace

kubectl delete --namespace=$namespace rc/kafka-broker
kubectl delete --namespace=$namespace rc/zookeeper-controller-1
kubectl delete --namespace=$namespace rc/zookeeper-controller-2
kubectl delete --namespace=$namespace rc/zookeeper-controller-3
kubectl delete --namespace=$namespace svc/kafka
kubectl delete --namespace=$namespace svc/zoo1
kubectl delete --namespace=$namespace svc/zoo2
kubectl delete --namespace=$namespace svc/zoo3

echo -n Waiting for all pods to terminate
while [ -n "`kubectl get pods --namespace=$namespace`" ]; do
  echo -n .
  sleep 3
done
echo
echo All pods have terminated

# remove namespace
#kubectl delete namespace/$namespace
