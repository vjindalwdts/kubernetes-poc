package com.wdtablesystems.ccas.service2;

import com.wdtablesystems.ccas.service1.Service1Client;
import com.wdtablesystems.ccas.service2.messaging.publish.Test1TopicMessagePublisher;
import com.wdtablesystems.common.messaging.Publisher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by vjindal on 10/17/16.
 */
@Controller
@RequestMapping(value = "/api/service2/v1")
public class Service2Controller {

    @Autowired
    private Service1Client service1Client;

    @Autowired
    private Test1TopicMessagePublisher publisher;

    @RequestMapping(method = RequestMethod.GET, value = "/greeting")
    public ResponseEntity<String> greet(@RequestParam(value = "name" ,required = false) String name) {
        String ret = "Hello " + name + " from Service2";
        ResponseEntity responseEntity = new ResponseEntity(ret, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/doublegreeting")
    public ResponseEntity<String> doubleGreet(@RequestParam(value = "name") String name) {
        String ret = "Hello " + name + " from Service2 And " + service1Client.greet(name);
        ResponseEntity responseEntity = new ResponseEntity(ret, HttpStatus.OK);
        return responseEntity;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/messagegreeting")
    public ResponseEntity<String> messagegreeting(@RequestParam(value = "name" ,required = false) String name) {
        publisher.publish(name);
        String ret = "Hello " + name + " from Service2";
        ResponseEntity responseEntity = new ResponseEntity(ret, HttpStatus.OK);
        return responseEntity;
    }
}
