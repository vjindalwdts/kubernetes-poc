package com.wdtablesystems.ccas.service2.messaging.publish;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

/**
 * Created by sgoel on 6/2/16.
 */

@Lazy
@Service("test1TopicMessagePublisher")
public class Test1TopicMessagePublisher extends AbsKafkaPublisher {

    public Test1TopicMessagePublisher() {
        super("test1");
    }
}