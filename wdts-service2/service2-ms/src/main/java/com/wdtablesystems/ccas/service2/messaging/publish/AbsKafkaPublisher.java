package com.wdtablesystems.ccas.service2.messaging.publish;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wdtablesystems.common.messaging.Publisher;
import com.wdtablesystems.common.messaging.kafka.AbstractKafkaPublisher;
import com.wdtablesystems.metrics.service.MetricsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;


public abstract class AbsKafkaPublisher extends AbstractKafkaPublisher implements Publisher {

    @Value("${spring.application.name}")
    private String source;


    @Value("${kafka.bootstrap.servers}")
    private String bootstrapServers;

    @Resource(name = "metricsService")
    private MetricsService metricsService;

    @Autowired
    private ObjectMapper objectMapper;


    private String topic;

    private String serviceName;


    public AbsKafkaPublisher(String topic) {
        this.topic = topic;
        this.serviceName = "Kafka_" + topic + "_Publisher";
    }


    /**
     * Service initialization code, called after the class is instantiated.
     * <p>
     * Initializes the Kafka producer and sets the default topic.
     */
    @PostConstruct
    public void init() {
        init(source, topic, metricsService, bootstrapServers);
    }

    /**
     * Close Kafka producer.
     */
    @PreDestroy
    public void close() {
        super.close();
    }


    @Override
    public String getSource() {
        return source;
    }


    @Override
    public String getBootstrapServers() {
        return bootstrapServers;
    }

    @Override
    public MetricsService getMetricsService() {
        return metricsService;
    }

    @Override
    public String getServiceName() {
        return serviceName;
    }

    public void setServiceName(String serviceName) {
        this.serviceName = serviceName;
    }

    @Override
    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public void publish(Object message) {
        publish(message,null);
    }

    public void publish(Object message, String partitionKey) {
        try {
            String messageStr = objectMapper.writeValueAsString(message);
            super.publish(messageStr, partitionKey);
        } catch (JsonProcessingException ex) {
            throw new RuntimeException("Unable to map to JSON: " + message, ex);
        }
    }


}
