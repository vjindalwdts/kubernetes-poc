#!/bin/bash

echo -n "Enter the namespace to teardown: "
read namespace

kubectl delete --namespace=$namespace rc/wdts-service2
kubectl delete --namespace=$namespace svc/wdts-service2

echo -n Waiting for all pods to terminate
while [ -n "`kubectl get pods  --selector=app=wdts-service2  --namespace=$namespace`" ]; do
  echo -n .
  sleep 3
done
echo
echo All pods have terminated

# remove namespace
#kubectl delete namespace/$namespace
