#!/bin/bash

namespace="wdts"
service_name="topology"
spring_application_name="topology-service"
service_version="5.1.0"
service_download_prefix="http://172.26.2.109:8080/job/location"

echo 'Deploying '$service_name
minikube ssh sudo 'mkdir -p /opt/wdts/code/'$service_name'/'$service_version
minikube ssh sudo 'mkdir -p /opt/wdts/code/'$service_name'/'$service_version'/logs'
minikube ssh sudo 'chmod -R 777 /opt/wdts/code/'$service_name'/'$service_version
MINIKUBE_IP=`minikube ip`
downlordUrl=$service_download_prefix'/lastStableBuild/com.wdtablesystems.ccas$'$service_name'-ms/artifact/com.wdtablesystems.ccas/'$service_name'-ms/'$service_version'-SNAPSHOT/'$service_name'-ms-'$service_version'-SNAPSHOT.jar'
echo 'Downloading jar '$downlordUrl
curl -O $downlordUrl
mv $service_name-ms-$service_version-SNAPSHOT.jar $service_name-ms.jar
scp $service_name-ms.jar docker@$MINIKUBE_IP:/opt/wdts/code/$service_name/$service_version
minikube ssh sudo 'chmod -R 777 /opt/wdts/code/'$service_name'/'$service_version
minikube ssh 'ls -ltr /opt/wdts/code/'$service_name'/'$service_version
rm $service_name-ms.jar


# Deploy zookeeper service
services=`kubectl get svc --namespace=$namespace`
if [[ $services =~ "$spring_application_name" ]]; then
  echo Deleting existing services
  kubectl delete --namespace=$namespace svc/$spring_application_name
fi
cat kubernetes/service.yml  | sed "s/_service_name_/$service_name/g"  | sed "s/_service_version_/$service_version/g"  | sed "s/_spring_application_name_/$spring_application_name/g" | sed "s/_NAMESPACE_/$namespace/g" | kubectl create -f -

# Deploy zookeeper replication controllers
cat kubernetes/rc.yml | sed "s/_service_name_/$service_name/g"  | sed "s/_service_version_/$service_version/g"  | sed "s/_spring_application_name_/$spring_application_name/g" | sed "s/_NAMESPACE_/$namespace/g" | kubectl create -f -

# Check that zoo1, zoo2 and zoo3 are running pods
# Wait for all zookeeper pods to be ready
for i in `kubectl get pods --selector=app=$spring_application_name --namespace=$namespace -o jsonpath='{.items[*].metadata.name}'`;
do
  echo -n "Waiting for pod ${i} to be ready"
  sleep 2 # Give container a chance to start
  while [ `kubectl get pod/${i} --namespace=$namespace -o jsonpath='{..status.containerStatuses[*].ready}'` != "true" ]; do
    echo -n .
    sleep 5
  done
  echo
  echo pod ${i} ready
done