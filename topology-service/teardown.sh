#!/bin/bash

namespace="wdts"
service_name="topology"
spring_application_name="topology-service"
service_version="5.1.0"
service_download_prefix="http://172.26.2.109:8080/job/location"



kubectl delete --namespace=$namespace rc/$spring_application_name
kubectl delete --namespace=$namespace svc/$spring_application_name

echo -n Waiting for all pods to terminate
while [ -n "`kubectl get pods  --selector=app=$spring_application_name --namespace=$namespace`" ]; do
  echo -n .
  sleep 3
done
echo
echo All pods have terminated

# remove namespace
#kubectl delete namespace/$namespace
