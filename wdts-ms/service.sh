#!/bin/bash

export JAVA_OPTS="-XX:+HeapDumpOnOutOfMemoryError -XX:HeapDumpPath=/opt/wdts/service/logs -ea -Dtime.zone=$TIME_ZONE -Duser.timezone=$TIME_ZONE -Dorg.apache.catalina.session.StandardSession.ACTIVITY_CHECK=true"
export COMMAND_OPTS="--server.port=80 --ribbon.eureka.enabled=false --datasource.voltdb.url=$VOLTDB_URL --datasource.voltdb.username=$VOLTDB_USERNAME --datasource.voltdb.password=$VOLTDB_PASSWORD --kafka.bootstrap.servers=$KAFKA_BROKERS --com.wdts.kafka.zookeeper.address=$ZOOKEEPER_SERVERS"

java ${JAVA_OPTS} -jar /opt/wdts/service.jar ${COMMAND_OPTS}