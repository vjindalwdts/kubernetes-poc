package com.wdtablesystems.ccas.service1;

import com.wdtablesystems.common.logging.Logger;
import com.wdtablesystems.common.logging.LoggerImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Date;

import static com.wdtablesystems.common.logging.KeyValueFactory.keyValue;


/**
 * Created by vjindal on 10/17/16.
 */
@Controller
@RequestMapping(value = "/api/service1/v1")
public class Service1Controller {

    private static final Logger logger = new LoggerImpl(Service1Controller.class);

    @RequestMapping(method = RequestMethod.GET, value = "/greeting")
    public ResponseEntity<String> greet(@RequestParam(value = "name" ,required = false) String name) {
        String ret = "1 Hello " + name + " from Service1";
        logger.error("greeting called",keyValue("name",name));
        ResponseEntity responseEntity = new ResponseEntity(ret, HttpStatus.OK);
        return responseEntity;
    }
}
