#!/bin/bash

minikube ssh sudo 'mkdir -p /opt/wdts/code/1.0'
minikube ssh sudo 'chmod -R 777 /opt/wdts/code/1.0'
MINIKUBE_IP=`minikube ip`
mvn clean install
cp service1-ms/target/service1-ms-5.1.0-SNAPSHOT.jar service1-ms.jar
scp service1-ms.jar docker@$MINIKUBE_IP:/opt/wdts/code/1.0
minikube ssh sudo 'chmod -R 777 /opt/wdts/code/1.0'
minikube ssh 'ls -ltr /opt/wdts/code/1.0'
rm service1-ms.jar

echo -n "Enter the namespace to use for deploying Service1: "
read namespace
namespaces=`kubectl get namespace -o jsonpath={..name}`
if ! [[ $namespaces =~ "$namespace" ]]; then
  echo "Setting up namespace: $namespace"
  kubectl create -f - <<EOF
apiVersion: v1
kind: Namespace
metadata:
  name: "$namespace"
EOF
else
  echo "Namespace '$namespace' already exists"
fi
echo

# Deploy zookeeper service
services=`kubectl get svc --namespace=$namespace`
if [[ $services =~ "wdts-service1" ]]; then
  echo Deleting existing services
  kubectl delete --namespace=$namespace svc/wdts-service1
fi
cat kubernetes/service.yml | sed "s/_NAMESPACE_/$namespace/" | kubectl create -f -

# Deploy zookeeper replication controllers
cat kubernetes/rc.yml | sed "s/_NAMESPACE_/$namespace/" | kubectl create -f -

# Check that zoo1, zoo2 and zoo3 are running pods
# Wait for all zookeeper pods to be ready
for i in `kubectl get pods --selector=app=wdts-service1 --namespace=$namespace -o jsonpath='{.items[*].metadata.name}'`;
do
  echo -n "Waiting for pod ${i} to be ready"
  sleep 2 # Give container a chance to start
  while [ `kubectl get pod/${i} --namespace=$namespace -o jsonpath='{..status.containerStatuses[*].ready}'` != "true" ]; do
    echo -n .
    sleep 5
  done
  echo
  echo pod ${i} ready
done
