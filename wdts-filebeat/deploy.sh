#!/bin/bash

echo -n "Enter the namespace to use for deploying wdts-filebeat: "
read namespace
namespaces=`kubectl get namespace -o jsonpath={..name}`
if ! [[ $namespaces =~ "$namespace" ]]; then
  echo "Setting up namespace: $namespace"
  kubectl create -f - <<EOF
apiVersion: v1
kind: Namespace
metadata:
  name: "$namespace"
EOF
else
  echo "Namespace '$namespace' already exists"
fi
echo

cat kubernetes/ds.yml | sed "s/_NAMESPACE_/$namespace/" | kubectl create -f -


for i in `kubectl get pods --selector=app=wdts-filebeat --namespace=$namespace -o jsonpath='{.items[*].metadata.name}'`;
do
  echo -n "Waiting for pod ${i} to be ready"
  sleep 2 # Give container a chance to start
  while [ `kubectl get pod/${i} --namespace=$namespace -o jsonpath='{..status.containerStatuses[*].ready}'` != "true" ]; do
    echo -n .
    sleep 5
  done
  echo
  echo pod ${i} ready
done
