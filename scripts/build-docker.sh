#!/bin/bash
eval $(docker-machine env dev)
ls ../
echo -n "Enter the docker image to build "
read dockername
REG_IP=`docker-machine ip registry`
set -x
docker build -t $dockername ../$dockername/.
docker tag $dockername $REG_IP:80/$dockername
docker push $REG_IP:80/$dockername
eval $(minikube docker-env)
REG_IP=`docker-machine ip registry`
docker pull $REG_IP:80/$dockername
set +x