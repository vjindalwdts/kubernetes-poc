#!/bin/bash
eval $(minikube docker-env)
echo -n "Enter the namespace to use "
read namespace

echo -n "Enter the component to use "
read cmp

POD_NAME=`kubectl get pods  --selector=app=$cmp --namespace=$namespace -o jsonpath='{.items[*].metadata.name}'`
echo $POD_NAME

echo -n -e "\033]0;$POD_NAME\007"

kubectl -it --namespace=$namespace exec $POD_NAME bash